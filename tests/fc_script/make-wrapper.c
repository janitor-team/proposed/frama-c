/* run.config
   NOFRAMAC: testing frama-c-script
   COMMENT: we must filter 'make:'/'gmake:' output lines, since they differ when run by the CI (e.g. mention to jobserver)
   EXECNOW: LOG make-wrapper.res LOG make-wrapper.err cd @PTEST_DIR@ && touch make-wrapper2.c && touch make-wrapper3.c && FRAMAC=../../bin/frama-c ../../bin/frama-c-script make-wrapper --make-dir . -f make-for-make-wrapper.mk | sed -E -e "s:$PWD:PWD:g;/^(real|user|sys)/d;" | grep -E -v "^g?make.*" | grep -v "recipe for target.*failed" | grep -v '^$' > result/make-wrapper.res 2> result/make-wrapper.err && rm -rf make-for-make-wrapper.parse make-for-make-wrapper.eva
*/

int defined(int a);

int specified(int a);

int external(int a);

int large_name_to_force_line_break_in_stack_msg(void) {
  return large_name_to_force_line_break_in_stack_msg();
}

int rec(void) {
  return large_name_to_force_line_break_in_stack_msg();
}

int main() {
  int a = 42;
  a = rec();
  a = defined(a);
  a = specified(a);
  a = external(a);
}
