(**************************************************************************)
(*                                                                        *)
(*  This file is part of the Frama-C's E-ACSL plug-in.                    *)
(*                                                                        *)
(*  Copyright (C) 2012-2021                                               *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  you can redistribute it and/or modify it under the terms of the GNU   *)
(*  Lesser General Public License as published by the Free Software       *)
(*  Foundation, version 2.1.                                              *)
(*                                                                        *)
(*  It is distributed in the hope that it will be useful,                 *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU Lesser General Public License for more details.                   *)
(*                                                                        *)
(*  See the GNU Lesser General Public License version 2.1                 *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).            *)
(*                                                                        *)
(**************************************************************************)

(** Handling errors. *)

exception Ignored
exception Typing_error of string
exception Not_yet of string
exception Not_memoized

type 'a or_error = Res of 'a | Err of exn

val untypable: string -> 'a
(** Type error built from the given argument. *)

val not_yet: string -> 'a
(** Not_yet_implemented error built from the given argument. *)

val ignored: unit -> 'a
(** Statement already signaled and marked as ignored *)

val not_memoized : unit -> 'a
(** @raise Not_memoized  when asking the preprocessed form of something that
    was not preprocessed *)

val handle: ('a -> 'a) -> 'a -> 'a
(** Run the closure with the given argument and handle potential errors.
    Return the provide argument in case of errors. *)

val generic_handle: ('a -> 'b) -> 'b -> 'a -> 'b
(** Run the closure with the given argument and handle potential errors.
    Return the additional argument in case of errors. *)

val nb_untypable: unit -> int
(** Number of untypable annotations. *)

val nb_not_yet: unit -> int
(** Number of not-yet-supported annotations. *)

val print_not_yet: string -> unit
(** Print the "not yet" message without raising an exception. *)

val retrieve_preprocessing: string -> ('a -> 'b or_error) -> 'a -> 'b
(** Retrieve the result of a preprocessing phase, which possibly failed.
    The [string] argument is used to display a message in case the preprocessing
    phase did not compute the required result. *)

(*
Local Variables:
compile-command: "make"
End:
*)
